@extends('layouts.index')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">DataTable with default features</h3>

        <a href="{{ route('admin.product.create') }}" class="btn btn-primary float-right">Create</a>

    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($product as $item_product)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$item_product->name}}</td>
                    <td>{{$item_product->price}}</td>
                    <td>{{$item_product->stock}}</td>
                    
                        <td>
                        <div class="row d-flex justify-content-center">
                            <div class="col-xs-4 m-1">
                                <a href='{{ route('admin.product.edit', $item_product) }}' class="btn btn-warning btn-sm">Edit</a>
                            </div>

                            <div class="col-xs-4 m-1">
                        <form action="{{ route('admin.product.delete', $item_product) }}" method="post">@csrf @method('delete')
                        <button type='submit' class="btn btn-danger btn-sm">Delete</button>
                    </div>

                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>
    <!-- /.card-body -->
</div>
@endsection

@push('js')
    <script>
        $('#example1').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });
    </script>
@endpush