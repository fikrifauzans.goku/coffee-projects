    
    @extends('layouts.index')
    
    @section('content')

    <form action="{{ route('admin.product.update' , $item_product->id) }}" method="post">@csrf
    <div class="form-group">
      <label for="exampleFormControlInput1">Product Name</label>
      <input name='name' type="text" class="form-control"  placeholder="create name" value="{{ $item_product->name }}"> 
    </div>
    <div class="form-group">
        <label for="exampleFormControlInput1">Product Descripton</label>
        <input name='description'  type="text" class="form-control"  placeholder="create name" value="{{ $item_product->description }}">
      </div>
    <div class="form-group">
        <label for="exampleFormControlInput1">Status</label>
        <input name='stock' type="text" class="form-control"  placeholder="create name" value="{{ $item_product->status }}">
    </div>
    <div class="form-group">
        <label for="exampleFormControlInput1">Price</label>
        <input name='price' type="number" class="form-control"  placeholder="create name" value="{{ $item_product->price }}">
    </div>
    <button type='submit' class="btn btn-primary">Submit</button>

    
</form>

@endsection
