<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.index');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index']);

Route::prefix('product')->middleware('auth')->group(function () {
    
    Route::get('/', [App\Http\Controllers\Admin\ProductController::class, 'index'])->name('admin.product.index');
    Route::get('/create', [App\Http\Controllers\Admin\ProductController::class, 'create'])->name('admin.product.create');
    Route::post('/store', [App\Http\Controllers\Admin\ProductController::class, 'store'])->name('admin.product.store');
    Route::get('/edit/{id}', [App\Http\Controllers\Admin\ProductController::class, 'edit'])->name('admin.product.edit');
    Route::post('/update/{id}', [App\Http\Controllers\Admin\ProductController::class, 'update'])->name('admin.product.update');
    Route::delete('/destroy/{id}', [App\Http\Controllers\Admin\ProductController::class, 'destroy'])->name('admin.product.delete');
});

Route::prefix('category')->middleware('auth')->group(function () {
    
    Route::get('/', [App\Http\Controllers\Admin\CategoryController::class, 'index'])->name('admin.Category.index');
    Route::get('/create', [App\Http\Controllers\Admin\CategoryController::class, 'create'])->name('admin.Category.create');
    Route::post('/store', [App\Http\Controllers\Admin\CategoryController::class, 'store'])->name('admin.Category.store');
    Route::get('/edit/{id}', [App\Http\Controllers\Admin\CategoryController::class, 'edit'])->name('admin.Category.edit');
    Route::post('/update/{id}', [App\Http\Controllers\Admin\CategoryController::class, 'update'])->name('admin.Category.update');
    Route::delete('/destroy/{id}', [App\Http\Controllers\Admin\CategoryController::class, 'destroy'])->name('admin.Category.delete');
    Route::get('/show/{id}', [App\Http\Controllers\Admin\CategoryController::class, 'show'])->name('admin.Category.show');
});
