<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $category = Category::all();
        return view('Admin.Category.index', compact('category'));
    }


    public function create()
    {
        return view('Admin.Category.create');
    }


    public function show($id){
        $category = Category::with('products')->find($id);
        return view('Admin.Category.show', compact('category'));
    
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Category::create($request->all());
        return redirect(route('admin.Category.index'));
    }

  
    public function edit($id)
    {
        $item_category = Category::find($id);
        return view('Admin.Category.edit', compact('item_category'));
    }


    public function update(Request $request, $id)
    {
        Category::find($id)->update($request->all());
        return redirect(route('admin.Category.index'));
    }


    public function destroy($id)
    {
        Category::destroy($id);
        return back();
    }
}
